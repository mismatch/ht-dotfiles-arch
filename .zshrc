zstyle :compinstall filename '/home/ht/.zshrc'
autoload -Uz compinit && compinit
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v

# options
setopt appendhistory                                            # immediately append history
setopt histignorealldups					# remove duplicates from history
setopt extendedglob                                             # extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # case insensitive globbing

# zstyle
zstyle ':completion:*' menu select				# arrow keys for completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}' 		# case insensitive completion
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on				# enable cache
zstyle ':completion:*' cache-path ~/.zsh/cache			# set cache location

alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

# left prompt
PROMPT="
%B%{%F{8}%}%(4~|%-1~/.../%2~|%~)%b%f 
%(?.%{%F{14}%}.%{%F{9}%})%(!.#.λ)%f "

# initalise zoxide
eval "$(zoxide init zsh)"

# source alias file
source ~/.local/share/htalias

# autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'

# auto start x server on login
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi

# make systemd aware of environment variables
# check with systemctl --user show-environment
systemctl --user import-environment PATH

# setup pyenv
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# set nvim as manpager
export MANPAGER="nvim +Man!"
