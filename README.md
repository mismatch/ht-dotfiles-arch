# dotfiles
## basic setup
rename / move / delete any files that have the same name as those that are in this repository

clone the repository into the home directory (since the working tree is $HOME)
```sh
git clone --bare https://gitlab.com/mismatch/ht-dotfiles-arch.git $HOME/.dotfiles.git
```

## alias
create an alias for using the dotfiles
```sh
echo 'alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"' >> $HOME/.zshrc
```

```sh
source ~/.zshrc
```

checkout the new repository (there will be conflicts if any of these files already exist)
```sh
dotfiles checkout
```

only show tracked files (rather than all files in the home directory)
```sh
dotfiles config --local status.showUntrackedFiles no
```

## gitignore
this is included in the repo

ignore all of the contents of the .dotfiles.git directory to avoid errors
```sh
echo ".dotfiles.git" >> $HOME/.gitignore
```

## use
use the alias `dotfiles` in the same way as normal `git` commands
```sh
dotfiles add -u
```
will add tracked files

## git aliases
useful aliases should go into `~/.gitconfig`
```sh
[alias]
    ch = checkout
    br = branch
    co = commit
    st = status
    unstage = reset HEAD --
    last = log -1 HEAD
    graph = log --graph
    gr = log --decorate --oneline --graph --all
    tracked = ls-tree -r HEAD --name-only
    alias = config --get-regexp ^alias\\.
    files = ls-files
    others = ls-files --others
    lg = log --oneline --graph --all
[diff]
    tool = nvimdiff
[difftool]
    prompt = false
[difftool "nvimdiff"]
    cmd = "nvim -d \"$LOCAL\" \"$REMOTE\""
```

```sh
git difftool
```
to show repo diffs in neovim
