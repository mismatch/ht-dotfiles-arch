# xdg environment variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export TERMINAL="alacritty"
export BROWSER="vivaldi-stable"
export EDITOR=/usr/bin/nvim

# pass passwords directory from ~/.password-store
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"

# add ~/.local/bin to the start of the $PATH
PATH="$HOME/.local/bin:$PATH"

# nnn settings
BLK="0B"
CHR="0B"
DIR="04"
EXEC="06"
FILE="00"
MULTIHARDLINK="06"
LINK="06"
MISSING="08" # details colour
ORPHAN="09"
FIFO="06"
SOCK="0B"
OTHER="06"

export NNN_FCOLORS="$BLK$CHR$DIR$EXEC$FILE$MULTIHARDLINK$LINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"
export NNN_COLORS='5214'
export NNN_TRASH=1
export NNN_USE_EDITOR=1

export npm_config_prefix="$HOME/.local"

# path to texlive binaries - put it at the front to take presidence over texlive installed via pacman
export PATH="/usr/local/texlive/2023/bin/x86_64-linux:$PATH"

# path to texlive documentation
export MANPATH="$MANPATH:/usr/local/texlive/2023/texmf-dist/doc/man"

export PATH="$PATH:/home/ht/.cargo/bin"
